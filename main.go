package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var buttonHTML = `
<div><button class="button" id="a" onclick="invoke(this)">a</button></div>
<div><button class="button" id="b" onclick="invoke(this)">b</button></div>
<div><button class="button" id="c" onclick="invoke(this)">c</button></div>
<div><button class="button" id="d" onclick="invoke(this)">d</button></div>
`

var preButtonHTML = `<!DOCTYPE html>
<html>
<head>
<style>
.container {
	display: flex;
  }
  .container.space-around {
	justify-content: space-around;
  }
  .button {
	width: 100px;
	height: 100px;
  }
</style>
</head>
<body>
<div class="container space-around">
`

var postButtonHTML = `</div>
<br/>
<input style="height:50px;width:300px" type='text' id='response' />
<script>
function invoke(button) {
	const response_input = document.getElementById('response');
	const url = '/buttons/' + button.id;
	fetch(url)
		.then(response => response.text())
		.then(text => response_input.value = text)
		.catch(error => console.warn(error));
}
</script>
</body>
</html>
`

// Index Return the home page
func Index(w http.ResponseWriter, r *http.Request) {
	var page []string
	page = append(page, preButtonHTML)
	page = append(page, buttonHTML)
	page = append(page, postButtonHTML)
	w.WriteHeader(200)
	fmt.Fprint(w, strings.Join(page, ""))
}

var (
	buttonProcessed = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "gronk_app_button_presses_total",
			Help: "The total number of invocations",
		},
		[]string{"button"},
	)
)

// ButtonsHandler Handle user buttons
func ButtonsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	buttonProcessed.With(prometheus.Labels{"button": vars["button"]}).Inc()
	log.Println(vars["button"])
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Hello %v\n", vars["button"])
}

func main() {

	// Handle request
	r := mux.NewRouter()
	r.HandleFunc("/", Index)
	r.HandleFunc("/buttons/{button}", ButtonsHandler)
	http.Handle("/", r)
	// Metrics handler for Prometheus to be able to scrape metrics
	http.Handle("/metrics", promhttp.Handler())

	// serve the app
	log.Printf("Server up on port 5000")
	log.Fatal(http.ListenAndServe(":5000", nil))
}
